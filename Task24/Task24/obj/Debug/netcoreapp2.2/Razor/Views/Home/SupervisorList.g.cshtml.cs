#pragma checksum "C:\Users\ehelle\Documents\KrakenRepos\School\Tasks\Task24\Task24\Task24\Task24\Views\Home\SupervisorList.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "e6ec20015f13ed12cca2b77d4f00ee8ca6dd11be"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_SupervisorList), @"mvc.1.0.view", @"/Views/Home/SupervisorList.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/SupervisorList.cshtml", typeof(AspNetCore.Views_Home_SupervisorList))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\ehelle\Documents\KrakenRepos\School\Tasks\Task24\Task24\Task24\Task24\Views\_ViewImports.cshtml"
using Task24;

#line default
#line hidden
#line 2 "C:\Users\ehelle\Documents\KrakenRepos\School\Tasks\Task24\Task24\Task24\Task24\Views\_ViewImports.cshtml"
using Task24.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e6ec20015f13ed12cca2b77d4f00ee8ca6dd11be", @"/Views/Home/SupervisorList.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d3ed43ca22c4ff33646264b6fa54dcb88fad95ab", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_SupervisorList : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<Task24.Models.Supervisor>>
    {
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 1 "C:\Users\ehelle\Documents\KrakenRepos\School\Tasks\Task24\Task24\Task24\Task24\Views\Home\SupervisorList.cshtml"
  
    Layout = null;

#line default
#line hidden
            BeginContext(27, 29, true);
            WriteLiteral("\r\n<!DOCTYPE html>\r\n\r\n<html>\r\n");
            EndContext();
            BeginContext(56, 104, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e6ec20015f13ed12cca2b77d4f00ee8ca6dd11be3680", async() => {
                BeginContext(62, 91, true);
                WriteLiteral("\r\n    <meta name=\"viewport\" content=\"width=device-width\" />\r\n    <title>CoachInfo</title>\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(160, 2, true);
            WriteLiteral("\r\n");
            EndContext();
            BeginContext(201, 269, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "e6ec20015f13ed12cca2b77d4f00ee8ca6dd11be4958", async() => {
                BeginContext(207, 81, true);
                WriteLiteral("\r\n    <div>\r\n        <h1>\r\n            List of supervisors!!\r\n\r\n        </h1>\r\n\r\n");
                EndContext();
#line 20 "C:\Users\ehelle\Documents\KrakenRepos\School\Tasks\Task24\Task24\Task24\Task24\Views\Home\SupervisorList.cshtml"
         foreach (var supervisor in Model)
        {

#line default
#line hidden
                BeginContext(343, 35, true);
                WriteLiteral("            <div>\r\n                ");
                EndContext();
                BeginContext(379, 15, false);
#line 23 "C:\Users\ehelle\Documents\KrakenRepos\School\Tasks\Task24\Task24\Task24\Task24\Views\Home\SupervisorList.cshtml"
           Write(supervisor.Name);

#line default
#line hidden
                EndContext();
                BeginContext(394, 46, true);
                WriteLiteral("\r\n                <hr />\r\n            </div>\r\n");
                EndContext();
#line 26 "C:\Users\ehelle\Documents\KrakenRepos\School\Tasks\Task24\Task24\Task24\Task24\Views\Home\SupervisorList.cshtml"
        }

#line default
#line hidden
                BeginContext(451, 12, true);
                WriteLiteral("    </div>\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(470, 11, true);
            WriteLiteral("\r\n</html>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<Task24.Models.Supervisor>> Html { get; private set; }
    }
}
#pragma warning restore 1591
