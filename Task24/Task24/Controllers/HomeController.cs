﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24.Models;

namespace Task24.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.CurrentTime = DateTime.Now;

            return View("HomePage");
        }

        public IActionResult SupervisorList()
        {
            List<Supervisor> supervisors = new List<Supervisor>{
                new Supervisor{Id = 123, Name = "Gustav", Level = "MKUltraLvl", IsAvailable = true},
                new Supervisor{Id = 123, Name = "Frank", Level = "LollypopLvl", IsAvailable = true},
                new Supervisor{Id = 123, Name = "Rudolf", Level = "JustWeakLvl", IsAvailable = true},
            };
            return View(supervisors);
        }


    }
}
